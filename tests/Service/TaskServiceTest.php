<?php

namespace App\Tests\Service;

use App\Entity\Task;
use App\Entity\User;
use App\Service\TaskService;
use App\Service\UserService;
use PHPUnit\Framework\TestCase;
use App\Message\TaskNotification;
use App\Exception\NotAllowedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class TaskServiceTest extends TestCase
{
    private UserService $userService;
    private User $user;
    private EntityManagerInterface $entityManager;
    private MessageBusInterface $bus;
    private const UUID = '0bf11e70-1ca5-4bc8-b599-cec2145613c9';
    private const TASK_CONTENT = [
        "name" => "test",
        "summary" => "this is a test"
    ];

    public function testTaskIsCreatedWithSuccess()
    {
        $this->userService
            ->expects($this->once())
            ->method('userHasPermissions')
            ->willReturn(true);
        
        $this->bus
            ->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Envelope($this->createMock(TaskNotification::class)));

        $taskService = new TaskService(
            $this->entityManager,
            $this->userService,
            $this->bus
        );

        $task = $taskService->createTask($this->user, self::TASK_CONTENT);

        $this->assertInstanceOf(Task::class, $task);
    }

    public function testTaskIsEditedWithSuccess()
    {
        $task = new Task();
        $task->setName('yada name');
        $task->setSummary('yada summary');
        $task->setUser($this->user);

        $this->userService
            ->expects($this->once())
            ->method('userHasPermissions')
            ->willReturn(true);

        $taskService = new TaskService(
            $this->entityManager,
            $this->userService,
            $this->bus
        );

        $taskService->editTask($this->user, $task, self::TASK_CONTENT);

        $this->assertSame($task->getName(), self::TASK_CONTENT['name']);
    }

    public function testUserCanGetTask()
    {
        $task = new Task();
        $task->setName('yada name');
        $task->setSummary('yada summary');
        $task->setUser($this->user);

        $this->userService
            ->expects($this->once())
            ->method('userHasPermissions')
            ->willReturn(true);
        
        $this->userService
            ->expects($this->once())
            ->method('isTaskFromUser')
            ->willReturn(true);

        $taskService = new TaskService(
            $this->entityManager,
            $this->userService,
            $this->bus
        );

        $result = $taskService->canUserGetTask($this->user, self::UUID, $task);
        $this->assertTrue($result);
    }

    public function testThrowsExceptionIfUserIsNotAllowedToPerformAction()
    {
        $this->expectException(NotAllowedException::class);

        $this->userService
            ->expects($this->once())
            ->method('userHasPermissions');

        $taskService = new TaskService(
            $this->entityManager,
            $this->userService,
            $this->bus
        );

        $taskService->createTask($this->user, self::TASK_CONTENT);
    }

    protected function setup(): void
    {
        $this->userService = $this->createMock(UserService::class);
        $this->user = $this->createMock(User::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->bus = $this->createMock(MessageBusInterface::class);
    }
}
