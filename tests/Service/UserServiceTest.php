<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Service\UserService;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    private User $user;
    private const UUID = '0bf11e70-1ca5-4bc8-b599-cec2145613c9';

    public function testManagerIsNotAllowedToCreateTask()
    {
        $this->user
            ->expects($this->once())
            ->method('getRole')
            ->willReturn('MANAGER');
        
        $userService = new UserService();
        $isAllowed = $userService->userHasPermissions($this->user, 'create');

        $this->assertFalse($isAllowed);
    }

    public function testManagerIsAllowedToDeleteTask()
    {
        $this->user
            ->expects($this->once())
            ->method('getRole')
            ->willReturn('MANAGER');
        
        $userService = new UserService();
        $isAllowed = $userService->userHasPermissions($this->user, 'delete');

        $this->assertTrue($isAllowed);
    }

    public function testTechnicianIsNotAllowedToDeleteTask()
    {
        $this->user
            ->expects($this->once())
            ->method('getRole')
            ->willReturn('TECHNICIAN');
        
        $userService = new UserService();
        $isAllowed = $userService->userHasPermissions($this->user, 'delete');

        $this->assertFalse($isAllowed);
    }

    public function testTechnicianIsAllowedToCreateTask()
    {
        $this->user
            ->expects($this->once())
            ->method('getRole')
            ->willReturn('TECHNICIAN');
        
        $userService = new UserService();
        $isAllowed = $userService->userHasPermissions($this->user, 'create');

        $this->assertTrue($isAllowed);
    }

    public function testNotRecognizedActionIsNotAllowed()
    {
        $this->user
            ->expects($this->once())
            ->method('getRole')
            ->willReturn('TECHNICIAN');
        
        $userService = new UserService();
        $isAllowed = $userService->userHasPermissions($this->user, 'hello');

        $this->assertFalse($isAllowed);
    }

    public function testTaskFromSameUserReturnsTrue()
    {
        $this->user
            ->expects($this->once())
            ->method('getUuid')
            ->willReturn(self::UUID);
        
        $userService = new UserService();
        $isFromUser = $userService->isTaskFromUser($this->user, self::UUID);

        $this->assertTrue($isFromUser);
    }

    public function testTaskFromDifferentUserReturnsFalse()
    {
        $this->user
            ->expects($this->once())
            ->method('getUuid')
            ->willReturn(self::UUID);
        
        $userService = new UserService();
        $isFromUser = $userService->isTaskFromUser($this->user, '59a7c235-3698-4a12-a9f0-c5f5675df7ba');

        $this->assertFalse($isFromUser);
    }

    protected function setup(): void
    {
        $this->user = $this->createMock(User::class);
    }
}
