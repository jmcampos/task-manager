<?php

namespace App\Tests\ValueObject;

use PHPUnit\Framework\TestCase;
use App\ValueObject\TaskSummary;
use App\Exception\InvalidException;

class TaskSummaryTest extends TestCase
{
    public function testItThrowsExceptionIfSummaryLenghtNotAllowed(): void
    {
        $this->expectException(InvalidException::class);

        $longText = str_repeat("this is a summmary test", 110);

        $this->assertTrue(strlen($longText) > 2500);

        new TaskSummary($longText);
    }

    public function testTaskSummaryReturnsValue(): void
    {
        $text = "This is a test summary";
        $summary = new TaskSummary($text);

        $this->assertSame($text, $summary->getValue());
    }
}
