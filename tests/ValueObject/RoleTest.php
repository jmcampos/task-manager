<?php

namespace App\Tests\ValueObject;

use App\ValueObject\Role;
use PHPUnit\Framework\TestCase;
use App\Exception\RoleNotAllowedException;

class RoleTest extends TestCase
{
    public function testItThrowsExceptionIfRoleNotAllowed(): void
    {
        $this->expectException(RoleNotAllowedException::class);

        $role = 'member';

        new Role($role);
    }

    public function testRoleReturnsValue(): void
    {
        $role = "manager";
        $userRole = new Role($role);

        $this->assertSame('MANAGER', $userRole->getValue());
    }
}
