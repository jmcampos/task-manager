<?php
declare(strict_types=1);

namespace App\Entity;

use Ramsey\Uuid\Uuid;
use DateTimeImmutable;
use DateTimeInterface;
use App\ValueObject\Role;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class User
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private string $uuid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="user")
     */
    private $tasks;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private string $firstname;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="string")
     */
    private string $role;

    /**
     * in email format
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->uuid = Uuid::uuid4()->toString();
        $this->tasks = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->updatedAt = new DateTimeImmutable();
        if (!isset($this->createdAt)) {
            $this->createdAt = $this->updatedAt;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setRole($role): void
    {
        $newRole = new Role($role);
        $this->role = $newRole->getValue();
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(?Task $task): void
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
        }
    }

    public function removeTask(Task $task): void
    {
        if (!$this->tasks->contains($task)) {
            return;
        }
        $this->tasks->removeElement($task);
    }
}
