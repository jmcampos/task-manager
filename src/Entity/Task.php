<?php
declare(strict_types=1);

namespace App\Entity;

use App\ValueObject\TaskSummary;
use Ramsey\Uuid\Uuid;
use DateTimeImmutable;
use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;
use SpecShaper\EncryptBundle\Annotations\Encrypted;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="uuid", type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     */
    private User $user;

    /**
    * @ORM\Column(type="string")
    */
    private string $name;

    /**
    * @Encrypted
    * @ORM\Column(type="text")
    */
    private string $summary;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private DateTimeInterface $updatedAt;

    
    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->uuid = Uuid::uuid4();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->updatedAt = new DateTimeImmutable();
        if (!isset($this->createdAt)) {
            $this->createdAt = $this->updatedAt;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function setSummary($summary): void
    {
        $this->summary = (new TaskSummary($summary))->getValue();
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }
}
