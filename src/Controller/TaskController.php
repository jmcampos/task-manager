<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Service\TaskService;
use App\Exception\TaskNotFoundException;
use App\Exception\UserNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TaskController extends AbstractController
{
    private TaskService $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @Route("/create", name="save_task", methods="POST")
     */
    public function create(Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['username']);
        }

        //save task
        $this->taskService->createTask($user, $content);

        return new Response("task created");
    }

    /**
     * @Route("/edit/{taskId}", name="edit_task", methods="PUT")
     */
    public function edit(Request $request, string $taskId): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['userUuid']);
        }

        $task = $this->getDoctrine()->getRepository(Task::class)->findOneBy([
            'uuid' => $taskId
        ]);

        if (!$task) {
            throw new TaskNotFoundException($taskId);
        }

        $this->taskService->editTask($user, $task, $content);

        return new Response("task updated");
    }

    /**
     * @Route("/task/{taskId}", name="get_task", methods="GET")
     */
    public function showTask(Request $request, string $taskId): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['userUuid']);
        }

        $task = $this->getDoctrine()->getRepository(Task::class)->findOneBy([
            'uuid' => $taskId
        ]);

        if (!$task) {
            throw new TaskNotFoundException($taskId);
        }
        
        $this->taskService->canUserGetTask($user, $content['userUuid'], $task);

        $response = [
            "taskName" => $task->getName(),
            "userUuid" => $task->getUser()->getUuid(),
            "taskSummary" => $task->getSummary()
        ];
        
        return new JsonResponse($response);
    }

    /**
     * @Route("/delete/{taskId}", name="delete_task", methods="DELETE")
     */
    public function delete(Request $request, string $taskId): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['userUuid']);
        }

        $this->taskService->deleteTask($user, $taskId);

        return new Response("task deleted");
    }

    /**
     * @Route("/list-all", name="list_tasks", methods="GET")
     */
    public function listAll(Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['userUuid']);
        }

        $tasks = $this->taskService->listAllTasks($user);

        return new JsonResponse($tasks);
    }

    /**
     * @Route("/user-tasks/{userUuid}", name="list_user_tasks", methods="GET")
     */
    public function userTasks(Request $request, string $userUuid): Response
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uuid' => $content['userUuid']
        ]);

        if (!$user) {
            throw new UserNotFoundException($content['userUuid']);
        }

        $tasks = $this->taskService->getUserTasks($user, $userUuid);

        return new JsonResponse($tasks);
    }
}
