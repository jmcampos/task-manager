<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Task;
use App\Entity\User;
use App\Message\TaskNotification;
use App\Exception\NotAllowedException;
use App\Exception\TaskNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\UserNotAllowedException;
use Symfony\Component\Messenger\MessageBusInterface;

class TaskService
{
    private EntityManagerInterface $entityManager;
    private UserService $userService;
    private MessageBusInterface $bus;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserService $userService,
        MessageBusInterface $bus
    ) {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
        $this->bus = $bus;
    }

    public function createTask(User $user, array $content): Task
    {
        $this->checkPermissions($user, 'create');

        $task = new Task();
        $task->setName($content['name']);
        $task->setSummary($content['summary']);
        $task->setUser($user);
        $this->save($task);

        // notify manager
        $this->bus->dispatch(
            new TaskNotification(
                $user->getFirstname(),
                $task->getName(),
                $task->getCreatedAt()->format('Y-m-d H:i:s')
            )
        );

        return $task;
    }

    public function editTask(User $user, Task $task, array $content): void
    {
        $this->checkPermissions($user, 'edit');

        $task->setName($content['name']);
        $task->setSummary($content['summary']);

        $this->save($task);
    }

    /** check if user can get single task */
    public function canUserGetTask(User $user, string $userUuid, Task $task): bool
    {
        $this->checkPermissions($user, 'show');

        if (!$this->userService->isTaskFromUser($user, $userUuid)) {
            throw new NotAllowedException();
        }

        return true;
    }

    /** get all tasks for a specific user */
    public function getUserTasks(User $user, string $uuid): array
    {
        $this->checkPermissions($user, 'list-own');

        if (!$this->userService->isTaskFromUser($user, $uuid)) {
            throw new NotAllowedException();
        }

        $data = [];

        $tasks = $this->entityManager->getRepository(Task::class)->findBy([
            'user' => $user->getId()
        ]);

        foreach ($tasks as $task) {
            $data[] = [
                "taskName" => $task->getName(),
                "userUuid" => $task->getUser()->getUuid(),
                "taskSummary" => $task->getSummary()
            ];
        }

        return $data;
    }

    /** list all tasks from all users */
    public function listAllTasks(User $user): array
    {
        $this->checkPermissions($user, 'list');

        $data = [];

        $taskRepository = $this->entityManager->getRepository(Task::class);

        $tasks = $taskRepository->findAll();

        foreach ($tasks as $task) {
            $data[] = [
                "taskName" => $task->getName(),
                "userUuid" => $task->getUser()->getUuid(),
                "taskSummary" => $task->getSummary()
            ];
        }

        return $data;
    }

    public function deleteTask(User $user, string $taskUuid): void
    {
        $this->checkPermissions($user, 'delete');

        $task = $this->findTask($taskUuid);

        $this->entityManager->remove($task);
        $this->entityManager->flush();
    }

    /** check if user has permissions to perform this action */
    private function checkPermissions(User $user, string $action): void
    {
        if (!$this->userService->userHasPermissions($user, $action)) {
            throw new UserNotAllowedException($user);
        }
    }

    private function findTask(string $uuid): Task
    {
        $taskRepository = $this->entityManager->getRepository(Task::class);
        
        $task = $taskRepository->findOneBy([
            'uuid' => $uuid
        ]);

        if (!$task) {
            throw new TaskNotFoundException($uuid);
        }

        return $task;
    }

    private function save(Task $task)
    {
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }
}
