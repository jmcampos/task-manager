<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\User;

class UserService
{
    /** actions allowed for manager role */
    private const MANAGER = ['list', 'delete'];

    /** actions allowed for technician role */
    private const TECHNICIAN = ['create', 'edit', 'show', 'list-own'];


    /** check if user has permissions to perform action */
    public function userHasPermissions(User $user, string $action): bool
    {
        $userActions = $this->getUserActions($user->getRole(), $action);

        if (!in_array($action, $userActions)) {
            return false;
        }

        return true;
    }

    /**
     * hate this name (brain stopped working...sooorrrrryyyy)
     * check if the current user is owner of the requested tasks
     */
    public function isTaskFromUser(User $user, string $uuid): bool
    {
        if ($user->getUuid() !== $uuid) {
            return false;
        }

        return true;
    }

    private function getUserActions(string $role, string $action): array
    {
        if ($role === 'MANAGER') {
            return self::MANAGER;
        }

        return self::TECHNICIAN;
    }
}
