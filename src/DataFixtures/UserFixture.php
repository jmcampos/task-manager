<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class UserFixture extends Fixture
{
    private array $rows = [
        [
            "firstname" => "John",
            "password" => "!Yada1#two",
            "role" => "technician",
            "username" => "john@email.com"
        ],
        [
            "firstname" => "Susan",
            "password" => "!Yada1#two",
            "role" => "manager",
            "username" => "susan@email.com"
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->rows as $row) {
            $user = new User();
            $user->setFirstname($row['firstname']);
            $user->setPassword($row['password']);
            $user->setRole($row['role']);
            $user->setUsername($row['username']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
