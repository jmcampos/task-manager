<?php
declare(strict_types=1);

namespace App\Listener;

use App\Exception\InvalidException;
use App\Exception\NotAllowedException;
use App\Exception\NotFoundExeption;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class OnKernelEventListener
{
    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof NotAllowedException) {
            $event->setResponse(new Response($exception->getMessage(), Response::HTTP_FORBIDDEN));

            return;
        }

        if ($exception instanceof InvalidException) {
            $event->setResponse(new Response($exception->getMessage(), Response::HTTP_BAD_REQUEST));

            return;
        }

        if ($exception instanceof NotFoundExeption) {
            $event->setResponse(new Response($exception->getMessage(), Response::HTTP_NOT_FOUND));

            return;
        }

        $event->setResponse(new Response('Error processing request', Response::HTTP_INTERNAL_SERVER_ERROR));
    }
}
