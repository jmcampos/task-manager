<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;

class RoleNotAllowedException extends NotAllowedException
{
    private const ERROR_MESSAGE = "Role %s is not allowed";

    public function __construct(string $role)
    {
        parent::__construct(sprintf(self::ERROR_MESSAGE, $role));
    }
}
