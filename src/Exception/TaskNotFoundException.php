<?php
declare(strict_types=1);

namespace App\Exception;

use App\Exception\NotFoundExeption;

class TaskNotFoundException extends NotFoundExeption
{
    private const ERROR_MESSAGE = "Task with id %d was not found";

    public function __construct(string $id)
    {
        parent::__construct(sprintf(self::ERROR_MESSAGE, $id));
    }
}
