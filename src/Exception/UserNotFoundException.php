<?php
declare(strict_types=1);

namespace App\Exception;

use App\Exception\NotFoundExeption;

class UserNotFoundException extends NotFoundExeption
{
    private const ERROR_MESSAGE = "User with provided id %s was not found";

    public function __construct(string $uuid)
    {
        parent::__construct(sprintf(self::ERROR_MESSAGE, $uuid));
    }
}
