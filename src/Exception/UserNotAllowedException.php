<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;
use App\Entity\User;

class UserNotAllowedException extends NotAllowedException
{
    private const ERROR_MESSAGE = "User %s is not allowed to perform this operation";

    public function __construct(User $user)
    {
        parent::__construct(sprintf(self::ERROR_MESSAGE, $user->getFirstname()));
    }
}
