<?php
declare(strict_types=1);

namespace App\Message;

class TaskNotification
{
    private string $userFirstname;
    private string $taskName;
    private string $createdAt;

    public function __construct(string $userFirstname, string $taskName, string $createdAt)
    {
        $this->taskName = $taskName;
        $this->createdAt = $createdAt;
        $this->userFirstname = $userFirstname;
    }

    public function getTaskName(): string
    {
        return $this->taskName;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUserFirstname(): string
    {
        return $this->userFirstname;
    }
}
