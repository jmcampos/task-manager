<?php
declare(strict_types=1);

namespace App\ValueObject;

use App\Exception\InvalidException;

class TaskSummary
{
    private string $summary;

    public function __construct(string $summary)
    {
        if (strlen($summary) > 2500) {
            throw new InvalidException("Max allowed characters for summary is 2500");
        }

        $this->summary = $summary;
    }

    /**
     * return summary value
     */
    public function getValue(): string
    {
        return $this->summary;
    }
}
