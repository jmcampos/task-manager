<?php
declare(strict_types=1);

namespace App\ValueObject;

use App\Exception\RoleNotAllowedException;

class Role
{
    private const ALLOWED_ROLES = ["MANAGER", "TECHNICIAN"];

    private string $role;

    public function __construct(string $role)
    {
        $formatedRole = strtoupper($role);

        if (!in_array($formatedRole, self::ALLOWED_ROLES)) {
            throw new RoleNotAllowedException($role);
        }

        $this->role = $formatedRole;
    }

    /**
     * return role value
     */
    public function getValue(): string
    {
        return $this->role;
    }
}
