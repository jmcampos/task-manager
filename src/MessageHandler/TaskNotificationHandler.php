<?php


namespace App\MessageHandler;

use App\Message\TaskNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class TaskNotificationHandler implements MessageHandlerInterface
{
    public function __invoke(TaskNotification $message)
    {
        $user = $message->getUserFirstname();
        $task = $message->getTaskName();
        $date = $message->getCreatedAt();

        print_r("The tech {$user} performed the task {$task} on date {$date}");
    }
}
