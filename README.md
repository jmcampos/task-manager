# Task Manager

Code challenge done for recruitment process

-------------------------

### notes about authentication
to bypass the absence of login, in every request body, i send the Uuid of the "logged" user (user performing the action).

### notes about authorization
Due to time constrains, I implemented a very simple way of dealing with the authorization logic.
With more time, i would probably implement Symfony Voters or/and a design pattern (strategy).

### notes about tests
I didn't write tests for all the scenarios of user permissions. Funcionality is tested and two scenarios for each user are tested.

----------------------------
## environment setup

### clone project
```
git@gitlab.com:jmcampos/task-manager.git
```

### run on Docker:
```
cd docker
docker-compose up -d
```

### access the docker container
```
docker exec -it task_manager.php bash
```

### composer install
```
composer install
```

### replace DB values (db_user and db_password) in .env or create a .env.local
```
db_user: root
db_password: root
```

 ### install database
```
bin/console doctrine:database:create
```

### run migrations
```
bin/console doctrine:migrations:migrate
```

### load fixtures
```
 bin/console doctrine:fixtures:load
```

### run consume for async messaging
```
 bin/console messenger:consume async
```
-------

## testing the api
a collection for postman is also included in this project. Import the file "Task_Manager.postman_collection.json" into your postman